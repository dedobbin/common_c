#pragma once

#define MAP_LEN 20
#define MAP_KEY_LEN 40
#define MAP_VALUE_LEN 100

//todo: make dynamic len
typedef struct map_t
{
    char keys[MAP_LEN][MAP_KEY_LEN];
    char values[MAP_LEN][MAP_VALUE_LEN];
    int len;
} map_t;


void map_insert(map_t* map, char* key, char* value);
char* map_find(map_t* map, char* key);
int* arr_map_str_to_int(char** input, int input_len, int (*callback)(char*,char**, int));
