#pragma once
#include <stddef.h>
#include <stdbool.h>

#if defined(_WIN32) || defined(_WIN64)
    #define EOL "\r\n"
#elif defined(__unix__) || defined(__linux__) || defined(__APPLE__) || defined(__FreeBSD__) || defined(__OpenBSD__)
    #define EOL "\n"
#else
    #define EOL "\n"
#endif

typedef struct str_segments_t
{
    char** items;
    size_t len;
} str_segments_t;

typedef struct str_segments_ints_t
{
    int* items;
    size_t len;
} str_segments_ints_t;

char* file_get_contents(char* file_path);
/**
 * Mutates input. Use a copy if you want to keep it intact.
 * This also implies string literals will cause a segfault, for they are read-only (yet c doesn't make them const char*)
 **/
str_segments_t str_split(char* input, const char* delimiter, int max);
str_segments_t str_split_multichartok(char* input, const char* delimiter, unsigned int max);
/**
 * Mutates input. Use a copy if you want to keep it intact.
 * This also implies string literals will cause a segfault, for they are read-only (yet c doesn't make them const char*)
 * TODO: combine this with str_split?
 **/
str_segments_ints_t str_split_ints(char* input, const char* delimiter, int max);
void deinit_str_segments(str_segments_t* input);
void deinit_str_segments_ints(str_segments_ints_t *input);
/**
 * As common with regex grouping, first item is entire match
 */
str_segments_t* regex_groups(char* source, char* regex_str, unsigned int max_groups);

bool is_hex_str(const char* const input);
bool is_num_str(const char* const input);