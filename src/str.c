#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <regex.h> 
#include "str.h"

char* file_get_contents(char* file_path)
{
    FILE* fp = fopen(file_path, "r");
    if (fp == NULL){
        fprintf( stderr, "Could not open file %s.\n", file_path);
        return NULL;
    }

    fseek(fp, 0, SEEK_END);
    size_t sz = ftell(fp) + 1;
    rewind(fp);
    char* buffer = malloc(sz);
    fread(buffer, 1, sz, fp);
    buffer[sz-1] = '\0';
    
    fclose(fp);
    return buffer;
}

str_segments_t str_split(char* input, const char* delimiter, int max)
{
    str_segments_t res = {.len = 0};
    res.items = malloc(sizeof(char*) * max);
    memset (res.items, 0, max * sizeof(char*));

    char* tok = strtok(input, delimiter);
    while(tok != NULL){
        int i = res.len;
        
        if (i >= max){
            fprintf( stderr, "Too many string segments to split, max is %d.\n", max);
            break;
        }

        res.len++;
        res.items[i] = malloc(strlen(tok)+1);
        strcpy(res.items[i], tok);
        tok = strtok(NULL, delimiter);
    }
    return res;
}

void deinit_str_segments(str_segments_t* input)
{
    for (unsigned int i = 0; i < input->len; i++)
    {
        free(input->items[i]);
        input->items[i] = NULL;
    }
    free(input->items);
    input->items = NULL;
    input->len = 0;
}

str_segments_ints_t str_split_ints(char* input, const char* delimiter, int max)
{
    str_segments_ints_t res = {.len = 0};
    res.items = malloc(sizeof(char*) * max);
    memset (res.items, 0, max * sizeof(char*));

    char* tok = strtok(input, delimiter);
    while(tok != NULL){
        int i = res.len;
        
        if (i >= max){
            fprintf( stderr, "Too many string segments to split, max is %d.\n", max);
            break;
        }

        res.len++;
        res.items[i] = atoi(tok);
        tok = strtok(NULL, delimiter);
    }
    return res;
}

void deinit_str_segments_ints(str_segments_ints_t *input)
{
    free(input->items);
    input->len = 0;
}

str_segments_t str_split_multichartok(char* input, const char* delimiter, unsigned int max)
{
    str_segments_t res = {.len=0};
    res.items = malloc(sizeof(char*)*max);

    char *ptr=input;
    while (1){    
        if (res.len == max){
            fprintf( stderr, "Too many string segments to split, max is %d.\n", max);
            break;
        }

        char *substr = strstr(ptr, delimiter);
        if (substr == NULL) break;
        
        int len = substr-ptr;
        res.items[res.len] = malloc(len+1);
        memcpy(res.items[res.len], ptr, len);
        res.items[res.len][len]='\0';
        res.len ++;
        ptr += len + strlen(delimiter);
        substr = strstr(ptr, delimiter);
        if (substr == NULL) break;
    }

    if (strlen(ptr) > 0 && res.len != max){
        res.items[res.len] = malloc(strlen(ptr)+1);
        strcpy(res.items[res.len], ptr);
        res.len ++;
    }

    return res;
}

str_segments_t* regex_groups(char* source, char* regex_str, unsigned int max_groups)
{
    regex_t regex_compiled;
    regmatch_t groupArray[max_groups];

    if (regcomp(&regex_compiled, regex_str, REG_EXTENDED)){
            fprintf( stderr, "Could not compile regular expression.\n");
        return NULL;
    }

    str_segments_t* groups = malloc(sizeof(str_segments_t));
    groups->len = 0;
    groups->items = malloc(max_groups * sizeof(char*));
    if (regexec(&regex_compiled, source, max_groups, groupArray, 0) == 0){
        unsigned int g = 0;
        for (g = 0; g < max_groups; g++){
            if (groupArray[g].rm_so == (size_t)-1){
                break;  // No more groups
            }

            char sourceCopy[strlen(source) + 1];
            strcpy(sourceCopy, source);
            char* match = sourceCopy + groupArray[g].rm_so;
            sourceCopy[groupArray[g].rm_eo] = 0;
            //TODO: also return pos
            //printf("Group %u: [%2u-%2u]: %s\n", g, groupArray[g].rm_so, groupArray[g].rm_eo, match);
            groups->items[g] = malloc(strlen(source) + 1);
            strcpy(groups->items[g], match);
            groups->len++;
        }
    }
    regfree(&regex_compiled);
    return groups;
}

bool is_hex_str(const char* const input)
{
    for (int j = 0; j < strlen(input); j++){
        if (!isxdigit(input[j])){
            return false;
        }
    }
    return true;
}

bool is_num_str(const char* const input)
{
    for (int j = 0; j < strlen(input); j++){
        if (!isdigit(input[j])){
            return false;
        }
    }
    return true;
}