#include <stdlib.h>
#include <string.h>
#include "map.h"

void map_insert(map_t* map, char* key, char* value)
{    
    //todo: input validation?
    int i = map->len;
    strcpy(map->keys[i], key);
    strcpy(map->values[i], value);

    map->len++;
}

char* map_find(map_t* map, char* key)
{
    for (int i = 0; i < map->len; i++)
    {
        if (strcmp(map->keys[i], key) == 0){
            return map->values[i];
        }
    }
    return NULL;
}

int* arr_map_str_to_int(char** input, int input_len, int (*callback)(char*,char**, int))
{
    int* res = malloc(input_len*sizeof(int));
    for (int i = 0; i < input_len; i++){
        if (!callback)
            res[i] = atoi(input[i]);
        else 
            res[i] = callback(input[i], input, input_len);
    }
    return res;
}