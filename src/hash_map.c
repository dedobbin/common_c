#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hash_map.h"

unsigned int hash(char* key){
    unsigned int hash = 0;
    while (*key){
        hash = hash * 31 + (*key++);
    }

    return hash % TABLE_SIZE;
}

Map* map_create(){
    Map* map = (Map*)malloc(sizeof(Map));
    if (!map){
        perror("Failed to create map");
        return NULL;
    }

    for (int i = 0; i < TABLE_SIZE; i++){
        map->table[i] = NULL;
    }

    return map;
}

bool map_insert(Map* map, char* key, Callback cb){
    unsigned int index = hash(key);
    Pair* new_pair = (Pair*)malloc(sizeof(Pair));
    if (new_pair == NULL){
        perror("Failed to insert into map");
        return false;
    }

    new_pair->key = strdup(key);
    new_pair->value = cb;
    new_pair->next = NULL;

    if (!map->table[index]){
        map->table[index] = new_pair;
    } else {
        printf("Hash collision..\n");
        Pair* current = map->table[index];
        while(current->next != NULL){
            current = current->next;
        }
        current->next = new_pair;
    }

    return true;
}

Callback map_get(const Map* const map, char* key){
    unsigned int index = hash(key);
    Pair* current = map->table[index];
    while(current){
        // TODO: optimize ??? would only make sense if there are alot of collisions though.
        //   at that point just make the map bigger?
        if (strcmp(current->key, key) == 0){
            return current->value;
        }

        if (current->next == NULL){
            fprintf(stderr, "Hash found, but correct key was not stored there. Something went wrong with the hashing\n");
            return NULL;
        }
        current = current->next;
    }
    return NULL;
}

void map_destroy(Map* map){
    for (int i = 0; i < TABLE_SIZE; i++){
        Pair* current = map->table[i];
        while (current){
            Pair* tmp = current;
            current = current->next;
            free(tmp->key);
            free(tmp);
        }
    }
    free(map);
}