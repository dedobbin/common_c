#include <time.h>
#include <stdio.h>
#include "timer.h"

static clock_t start;

void timer_start()
{
    start = clock();
}

double timer_end(bool do_print)
{
    clock_t end = clock();
    double seconds = ((double)(end - start)) / CLOCKS_PER_SEC;
    if (do_print){
        printf("Measures: %.6f s\n", seconds);
    }
    return seconds;
}