#include <stdlib.h>
#include <stdbool.h>
#include "old_tests.h"

void run_old_tests(bool do_run_file_tests)
{
    printf("old_str_split_tests\n");
    old_str_split_tests();
    printf("old_map_tests\n");
    old_map_tests();
    printf("old_regex_tests\n");
    old_regex_tests();

    if (do_run_file_tests){
        printf("old_file_tests\n");
        old_file_tests();
    } else {
        printf("old_file_tests SKIPPED, provide --old_file_tests to run\n");
    }
}

int main(int argc, char *argv[])
{
    bool do_run_file_tests = false;
    for (int i = 1; i < argc; i++){
        if (strcmp("--old_file_tests", argv[i]) == 0){
            do_run_file_tests = true;
        }
    }

    run_old_tests(do_run_file_tests);
}